<?php
    session_start();
    $colors = array( 'black' => "#000000", 
                    'white' => "#ffffff", 
                    'red' => "#ff0000", 
                    'blue' => "#0000ff"
            );
    $backgroundName = $_POST['background'];
    $foregroundName = $_POST['foreground'];
    $_SESSION['bg'] = $colors[$backgroundName];
    $_SESSION['fg'] = $colors[$foregroundName];
?>
<html>
<head><title>Preferences Set</title></head> 
<body>
<p>Thank you. Your preferences have been changed to:<br /> 
Background: <?php echo $_SESSION['bg']; ?><br /> 
Foreground: <?php echo $_SESSION['fg']; ?></p>
<p>Click <a href="prefs_demo.php">here</a> to see the preferences in action.</p>
</body>