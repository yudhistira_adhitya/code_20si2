<?php

require "config.php";

$id = $_GET['id'];

$query = "SELECT * FROM kontak WHERE id=?";
$stmt = $conn->prepare($query);
$stmt->bind_param("d",$id);
$stmt->execute();
$hasil = $stmt->get_result();
$row = $hasil->fetch_assoc();
// var_dump($row);die;
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Data Kontak</title>
</head>
<body>
	<h2>Edit Kontak</h2>
	<form action="edit_kontak_proses.php" method="post">
		Nama : <input type="text" name="nama" value="<?= $row['nama'] ?>" disabled><br/>
        Alamat : <textarea name="alamat"><?= $row['alamat'] ?></textarea><br/>
		No. Telp : <input type="text" name="no_hp" value="<?= $row['no_hp'] ?>"><br/>
        <input type="hidden" name='id' value="<?= $row['id'] ?>"/>
		<input type="submit">
	</form>
</body>
</html>