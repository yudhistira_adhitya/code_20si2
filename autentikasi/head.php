<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Index</title>

    <style>
        header {
            width: 100%;
            height: 100px;
            background-color: yellow;
            text-align: center;
        }

        .container {
            width: 900px;
            margin: 0 auto;
            padding-top: 0;
        }

        h1 {
            margin: 0;
            padding: 10px 0;
        }

        nav {
            float: left;
            width: 28.5%;
            min-height: 500px;
            background-color: #ccc;

        }

        section {
            float: right;
            width: 70%;
            min-height: 500px;
            background-color: #333;
            color: #fff;
            padding: 5px;
        }

        ol li {
            list-style-type: none;
            width: 100%;
            text-align: center;
            border-top: 2px solid #fff;
            padding: 20px 0;
            background-color: #432;
            cursor: pointer;
        }

        ol li:hover {
            background-color: red;
        }

        li a {
            text-decoration: none;
            color: #fff;
            text-align: center;
        }

        ol {
            padding: 0;
            margin: 0;
        }

        label {
            text-align: right;
            width: 200px;
        }
    </style>