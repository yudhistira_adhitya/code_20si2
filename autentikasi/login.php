<!DOCTYPE html>
<html lang="en">

<head>
    <?php require('head.php'); ?>
</head>

<body>
    <div class="container">
        <?php require('header.php'); ?>

        <section>
        <section>
                <h1>Halaman Login</h1>
                <p>Silahkan login dengan akun anda</p>
                <form action='login_check.php' method='POST'>
                    <div>    
                        <label>Username : </label>
                        <input type="text" name='username' required />
                    </div>
                    <div>    
                        <label>Password : </label>
                        <input type="password" name='password' required />
                    </div>
                    <div>    
                        <input type="submit" name='submit' value='login' />
                    </div>
                </form>
        </section>  
        </section>
    </div>
</body>

</html>